
var express = require('express');
var router = express.Router();
var request = require('request');
var http = require('https');
var firebase = require('firebase');
var R = require('ramda');

const NodeCache = require( "node-cache" );
const myCache = new NodeCache();
require('firebase/database');

var logs = require("../log.js");
var config = require('../config');
var cache = {};
var startDateTime;
var runnersDatacacheId;
var ResumeId;
var consoleRunners = {};
var consoleIntervals = {};
firebase.initializeApp(config.livefbconfig);
// links to my firebase database
var db = firebase.database();

const options = {
    host: 'app.liverunapp.com',
    port: 443,
    path: '/api/',
    headers: {
        authorization: ''
    }
};

const post_options = {
    host: 'app.liverunapp.com',
    port: 443,
    method: 'PUT',
    path: '',
    headers: {
        'authorization': '',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
};


/* GET Version. */
router.get('/', function (req, res, next) {
    logs.start("Getting version...")
    logs.info("Getting Version now...");
    logs.stop("Getting version...")
    res.send('Version 8.8');    
});

//Get All Runners From FireBase , Calculate parameters and update in Vue Table 
router.get('/updateRace/:raceId',  function (req, res, next) {    
    try {
        
        const raceId = req.params.raceId;
        var loadRunners = req.query.loadRunners;

        if(typeof(loadRunners)== "undefined")
            loadRunners = 0;
        logs.info('UpdateRace API is started for Race: '+raceId)
        const token =  req.header('Authorization');
        const size = Object.keys(consoleRunners).length;
        if(size > 0){
            for (var key in consoleRunners) {
                if (consoleRunners.hasOwnProperty(key)) {
                    if(consoleRunners[key].createdOn!=undefined){
                        const totalMinutesSinceRaceStarted =   timeLeft(consoleRunners[key].createdOn);
                        if(totalMinutesSinceRaceStarted>1){
                            delete consoleRunners[key];
                        }
                    }
                }
            }
        }

        var noRaceInfo = (consoleRunners[raceId] === undefined || consoleRunners[raceId].raceData ==undefined)  && token !=undefined && raceId !=undefined;
        if(noRaceInfo ){ 
            consoleRunners[raceId] = {};
            consoleIntervals[raceId] ={};
            consoleRunners[raceId].timerStarted = 0;
            consoleRunners[raceId].timeCounter = 0;
            consoleIntervals[raceId].ccount = 0;
    
            consoleRunners[raceId].isRequestComplete = true;  

            if(loadRunners === "1"){
                db.ref('racers').child(raceId).child('status').once('value').then(function(status) {
                    // The Promise was "fulfilled" (it succeeded).
                    consoleRunners[raceId].raceStatus = status.val();
                }, function(error) {
                    // The Promise was rejected.
                    console.error(error);
                });

                db.ref('racers').child(raceId).child('statusLive').once('value').then(function(status) {
                    // The Promise was "fulfilled" (it succeeded).
                    consoleRunners[raceId].raceLiveStatus = status.val();
                }, function(error) {
                    // The Promise was rejected.
                    console.error(error);
                });
                if(consoleRunners[raceId] != undefined){

                    // if(consoleRunners[raceId].raceLiveStatus === "finished") {   
                    //     clearInterval(consoleIntervals[raceId].startRaceInterval)               
                    // }
                    // consoleRunners[raceId].isRequestComplete = true;  
                
                    // if(consoleRunners[raceId].isRequestComplete){
                        // logs.start("Getting Race Runners Data ..."+raceId)
                    //     consoleRunners[raceId].isRequestComplete = false;
                    //     try {
                    //         logs.info('started the Race interval!' + raceId)
                             startRaceInterval(token,raceId, function(){
                                res.send(consoleRunners[raceId]);
                                delete consoleRunners[raceId]
                             });
                        // }
                        // catch (error){
                        //     logs.error(error)
                        // }
                        
                    // }
                }


            }else{
                consoleRunners[raceId].startRaceCounter = 0;
                consoleIntervals[raceId].startRaceInterval = setInterval(function(){  
    
                    db.ref('racers').child(raceId).child('status').once('value').then(function(status) {
                        // The Promise was "fulfilled" (it succeeded).
                        consoleRunners[raceId].raceStatus = status.val();
                    }, function(error) {
                        // The Promise was rejected.
                        console.error(error);
                    });

                    db.ref('racers').child(raceId).child('statusLive').once('value').then(function(status) {
                        // The Promise was "fulfilled" (it succeeded).
                        consoleRunners[raceId].raceLiveStatus = status.val();
                    }, function(error) {
                        // The Promise was rejected.
                        console.error(error);
                    });
                    if(consoleRunners[raceId] != undefined){

                        if(consoleRunners[raceId].raceLiveStatus === "finished") {   
                            clearInterval(consoleIntervals[raceId].startRaceInterval)               
                        }
                        // consoleRunners[raceId].isRequestComplete = true;  
                    
                        if(consoleRunners[raceId].isRequestComplete){
                            // logs.start("Getting Race Runners Data ..."+raceId)
                            consoleRunners[raceId].isRequestComplete = false;
                            try {
                                logs.info('started the Race interval!' + raceId)
                                // startRaceInterval(token,raceId);
                                
                                startRaceInterval(token,raceId, function(){
                                    //Send repsonse only on first request
                                      consoleRunners[raceId].startRaceCounter++;
                                      if(consoleRunners[raceId].startRaceCounter<2){
                                          res.send(consoleRunners[raceId]);
                                      }

                                 });
                            }
                            catch (error){
                                logs.error(error)
                            }
                            
                        }else{
                            logs.info('Not able to complete the request!!!!!')
                            consoleRunners[raceId].isRequestComplete =true
                           
                        }
                    }

            
                },5000)
            }
            
        };
        if(!noRaceInfo){
            console.log('response sending to client for ' + raceId)

            res.send(consoleRunners[raceId]);
   
                if(consoleRunners[raceId].raceLiveStatus === 'finished') {          
                    consoleIntervals[raceId].ccount++;
                    if(consoleIntervals[raceId].ccount===1){
                        consoleRunners[raceId].createdOn = new Date();
                    }
                }
               
          
        }
        else{
            //res.end();
        }
    } 
    
    catch(error) {
        logs.error(error)
        logs.warning('error occured in raceID:'+ raceId)
    }
})

function startRaceInterval(token, raceId, _callback){
      
    try{
        logs.info('start race Interval has started for Race Id: '+ raceId )
        //TO UPDATE CHANGE IN RANK AFTER 5 MINS
        consoleRunners[raceId].timeCounter += 1.5;
        if (consoleRunners[raceId].timeCounter > 300) {
            consoleRunners[raceId].timeCounter = 0;
        }

        const myraceParams = {
            raceId: raceId,
            authorization: token
        }


        var getRaceDuration = new Promise(function (resolve, reject) {
            getRaceInfo(myraceParams, (data) => {
                resolve(JSON.parse(data).duration);
            })
        });
        getRaceDuration.then(function (promishedDuration) {
            consoleRunners[raceId].raceDuration = promishedDuration

                // Update current time in fb
            if(consoleRunners[raceId].raceStatus === 'running'){
                if(consoleRunners[raceId].timerStarted === undefined){
                    consoleRunners[raceId].timerStarted = 0;
                }


                var getStartDateTime = new Promise(function (resolve, reject) {
                    db.ref('racers').child(raceId).child('startDateTime').on('value', (startTime) => {
                        resolve(startTime.val());
                    });
                });
                getStartDateTime.then(function (promishedStartDateTime) {
                    if(promishedStartDateTime === null){
                        promishedStartDateTime = Date.now();
                    }
                    startDateTime = promishedStartDateTime;
                
                    if(consoleRunners[raceId].timerStarted === 0){
                        var currentTimeDependency = {
                            'raceDuration':consoleRunners[raceId].raceDuration,
                            'startDateTime':startDateTime,
                            'raceId':raceId,
                            'token':token
                        };
                        myCache.set( 'fbRaceInfo', currentTimeDependency, function( err, success ){
                            if(!err && success){
                                consoleRunners[raceId].timerStarted = 1;
                                setCurrentTime()
                            }
                        });
                        
                    }
                })
            }

            db.ref('racers').child(raceId).child('currentTime').on('value', (currentTime) => {
                consoleRunners[raceId].currentFbTime = currentTime.val()?currentTime.val():0
            });
        
            var getRunnersFromFirebase = new Promise(function (resolve, reject) {
                db.ref('racers').child(raceId).child("runners").once('value', (fbRunners) => {
                    const race = fbRunners.val();
                    const runners = []
                    if (race != null)
                        Object.keys(race).forEach((key) => {
                            race[key].id = parseInt(key);
                            runners.push(race[key]);
                        })
                    resolve(runners);
                })
            });
            
            var racerInfo = {};
            var raceData ={
                racerInfos: [],
                apiBroken:false
            }


            getRunnersFromFirebase.then(function (fireBaseRunners) {

                const raceParams = {
                    raceId: raceId,
                    authorization: token
                }

                
                consoleRunners[raceId].totalFbRunners = fireBaseRunners.length


                getRunner(raceParams, (data) => {
                    var dbRunners = JSON.parse(data);
                        dbRunners = dbRunners.filter(function( e ) {
                                return e !== null;
                            });
                            // if(dbRunners.length === 0){
                            //     dbRunners = fireBaseRunners.filter(function( e ) {
                            //         return e !== null;
                            //     });
                            // }
                    var runnerIds = R.project(['id'], dbRunners);
                    var ids = runnerIds.map(a => a.id);
                    var raceParams = {
                        raceId: raceId,
                        authorization: token,
                        runnerIds: JSON.stringify(ids),
                        raceStatus:consoleRunners[raceId].raceStatus
                    }
                    
                 try{
                    logs.info('Started getting Runners profile Data for RaceId:'+ raceId)
                    getRunnersdata(raceParams, function (runnerInfos,error) {
                        var finalRunners = [];
                        var runnerIds = [];
                        var runnerIdsLength = JSON.parse(raceParams.runnerIds).length;

                        //check if runner details api is broken or currently there is no runner in race

                        if(error && runnerIdsLength == 0){
                            consoleRunners[raceId].apiBroken = false;
                        runnerInfos =  [];
                        }else if(error){
                            logs.warning('Runners Profile data API is Broken!')
                        consoleRunners[raceId].apiBroken = true;
                        runnerInfos =  [];
                        }else{
                            consoleRunners[raceId].apiBroken = false;
                        runnerInfos =  runnerInfos;
                        }
                        
                        
                        const findRunnerInDBById = (id) => {
                            return runnerInfos.find((eachRunner) => eachRunner.runner.id == id)
                        }

                        for (var i = 0; i < dbRunners.length; i++) {
                            var runnerId = dbRunners[i].id;
                            var fbRunner = R.find(R.propEq('id', runnerId))(fireBaseRunners)
                            var finalRunner = dbRunners[i];
                            var runnerInfo = findRunnerInDBById(runnerId);
                            if (typeof runnerInfo !== 'undefined') {
                                finalRunner = R.merge(dbRunners[i], fbRunner);
                            }



                            if (typeof runnerInfo !== 'undefined') {
                                // runnerInfo.id = runnerInfo.runner.id;
                                finalRunner = R.merge(finalRunner, runnerInfo);
                            }

                        //This Piece of code is to check if Api has Question Answers or not
                        //meant to be modified in future
                            if (finalRunner.questionAnswer === null || finalRunner.questionAnswer === undefined) {
                                delete finalRunner.questionAnswer
                            }else {
                                finalRunner['questionAnswer'] = finalRunner.questionAnswer.answer;
                            }
                            if ( finalRunner.postRunAnswer === null || finalRunner.postRunAnswer === undefined) {
                                delete finalRunner.postRunAnswer
                            }else{
                                finalRunner['postRunAnswer'] = finalRunner.postRunAnswer.answer;
                            }

                            if (typeof runnerInfo !== 'undefined') {
                                finalRunner = R.merge(finalRunner, fbRunner);
                            }

                            //check if final runner has distance 

                            if (typeof finalRunner.distance == 'undefined') {
                                finalRunner['distance'] = 0;
                            }
                            
                              
                            finalRunners.push(finalRunner);
                            runnerIds.push(finalRunner.id);
                        }
                        var unsubscribedrunners = includeAllrunners(dbRunners,fireBaseRunners);
                        
                        if(unsubscribedrunners.length != 0){
                            unsubscribedrunners.forEach(runner => {

                                    runner['unsubscribed'] = true;
                                    finalRunners.push(runner);
                                    
                            });
                        }
                    
                        if (finalRunners != null) {
                            var lastRunnerDistance = 0;
                            var count = 0;
                            var changeInRank;
                            var changeInAltitude;
                            const byDistance = R.descend(R.prop('distance'))
                            const runnersSortByDistance = R.sort(byDistance, finalRunners)
                            var prevRaceData = [];
                            if (typeof cache[raceId] != 'undefined') {
                                prevRaceData = cache;
                            }

                            var position = 0;
                            R.forEachObjIndexed((runner, key) => {
                                count++;
                                //Get Distance  
                                var fbDistance = runner.distance ? runner.distance : 0;
                                var distance = (fbDistance / 1000).toFixed(2)
                                var prevRunnerData;
                                if (Object.keys(prevRaceData).length > 0) {
                                    prevRunnerData = R.find(R.propEq('id', runner.id))(prevRaceData[raceId]);
                                }

                                //Calculate Altitude

                                if (prevRunnerData != undefined) {
                                    if (consoleRunners[raceId].timeCounter < 3) {
                                        var finalAlt = runner.finalAltitude ? runner.finalAltitude : 0;
                                        changeInAltitude = finalAlt - prevRunnerData.altitude;
                                        changeInAltitude = changeInAltitude.toFixed(2);
                                    } else {
                                        changeInAltitude = prevRunnerData.finalAltitude ? prevRunnerData.finalAltitude : 0;
                                    }

                                } else {
                                    changeInAltitude = 0;
                                }

                                //Calculate Gap
                                var gap;
                                if (count == 1) {
                                    gap = 0
                                } else {
                                    gap = lastRunnerDistance - distance;
                                }
                                lastRunnerDistance = distance;
                                racerInfo = {};
                                //Update the position in firebase if runner is in firebase
                                var isInFB = R.find(R.propEq('id', runner.id))(fireBaseRunners);
                                if (typeof isInFB != 'undefined') {
                                        position++;

                                        db.ref('racers').child(raceId).child("runners").child(runner.id).update({
                                            "position": position
                                        })
                                    racerInfo.isRunning = true;
                                    racerInfo.rank = position;
                                    if (prevRunnerData != undefined) {
                                        if (consoleRunners[raceId].timeCounter < 3) {
                                            if (prevRunnerData.oldPosition == 0) {
                                                racerInfo.changeInRank = 0;
                                            } else {
                                                racerInfo.changeInRank = prevRunnerData.oldPosition - (racerInfo.rank == "-" ? 0 : racerInfo.rank);
                                            }
                                            if (racerInfo.changeInRank > 0) {
                                                racerInfo.changeInRank = "+" + racerInfo.changeInRank;
                                            }
                                        } else {
                                            racerInfo.changeInRank = prevRunnerData.changeInRank;
                                        }
            
                                    } else {
                                        racerInfo.changeInRank = 0;
                                    }
                                } else {
                                    racerInfo.isRunning = false;
                                    racerInfo.rank = "-";
                                    racerInfo.changeInRank = 0;                            
                                }

                                //Set old position & altitude in cache 
                                if (consoleRunners[raceId].timeCounter > 0 && consoleRunners[raceId].timeCounter < 3) {
                                    racerInfo.oldPosition = runner.position ? runner.position : position == "-" ? 0 : position;
                                    racerInfo.altitude = runner.finalAltitude ? runner.finalAltitude : 0;
                                } else {
                                    racerInfo.oldPosition = prevRunnerData ? prevRunnerData.oldPosition : 0;
                                    if (typeof prevRunnerData != "undefined") {
                                        racerInfo.altitude = prevRunnerData.altitude ? prevRunnerData.altitude : 0;
                                    } else {
                                        racerInfo.altitude = 0;
                                    }
                                }

                                // ****Get currentRacetotalPace ****

                                var currentRacetotalPace = null;
                    
                                if(runner.distance != 0){
                                var currentTimeInminutes  = consoleRunners[raceId].currentFbTime/60;
                                var elapsedTime = consoleRunners[raceId].raceDuration-currentTimeInminutes;

                                var totalPaceToCheck = runner.distance/elapsedTime;
                            
                                currentRacetotalPace = pace(runner.distance, elapsedTime);
                                }else{
                                    currentRacetotalPace = 0;
                                }
                                

                                //Check if runner is new
                                var isNewRunner = false
                                if(runner.totalRuns === 0){
                                    isNewRunner = true
                                }


                                //To check either currentTotal pace is greater than best records totalPace 
                                var isTotalPaceGreater;
                                var runnerBestRecords;
                                if(runner.bestByDuration!= undefined){
                                    runnerBestRecords = runner.bestByDuration[consoleRunners[raceId].raceDuration]?runner.bestByDuration[consoleRunners[raceId].raceDuration]:[];
                                }else{
                                    runnerBestRecords = [];
                                }
                                
                            
                                var recordTP = []; 

                                if (runnerBestRecords.length>0) {

                                    runnerBestRecords.forEach(record => { 

                                        var recordTotalpace =    record.distance/consoleRunners[raceId].raceDuration;
                                        recordTP.push(recordTotalpace)   

                                    });                            
                                }
                                
                                var maxRecordTP;

                                if (recordTP.length == 0) {

                                    isTotalPaceGreater = false;

                                } else {

                                    maxRecordTP = Math.max.apply(null, recordTP);

                                    if (totalPaceToCheck > maxRecordTP) {

                                        isTotalPaceGreater = true;   

                                    } else {

                                        isTotalPaceGreater = false;

                                    }
                                }

                                var bestRecordDistance;
                                var bestTotalPace;

                                if (maxRecordTP != undefined) {

                                    bestRecordDistance = maxRecordTP*consoleRunners[raceId].raceDuration;
                                    bestTotalPace = pace(bestRecordDistance, consoleRunners[raceId].raceDuration);

                                } else {
                                    bestRecordDistance = 0;
                                    bestTotalPace = 0;
                                }

                                var totalPace  = bestTotalPace/currentRacetotalPace*100;

                                if (!isFinite(totalPace)) {
                                    totalPace = 0;
                                }
                                // if(runner.questionAnswer){
                                //     if(runner.questionAnswer.answer){
                                //          var oldPreAnswer = runner.questionAnswer.answer
                                //     }
                                // }
                                // if(runner.postRunAnswer){
                                //     if(runner.postRunAnswer.answer){
                                //         var oldPostAnswer = runner.postRunAnswer.answer
                                //     }
                                // }
                
                                racerInfo.firstName = runner.firstName?runner.firstName:runner.name;
                                racerInfo.id = runner.id;
                                racerInfo.lastName = runner.lastName ? runner.lastName : '-';
                                racerInfo.distance = distance;
                                racerInfo.totalPace = parseInt(totalPace)+'%';
                                racerInfo.isTotalPaceGreater = isTotalPaceGreater;
                                //convert km to m
                                gap = gap.toFixed(2) / 0.0010000;
                                racerInfo.gap = parseInt(gap.toFixed(2));
                                racerInfo.pace = runner.pace ? runner.pace : '-';
                                racerInfo.finalAltitude = changeInAltitude ? parseInt(changeInAltitude) : 0;
                                racerInfo.gender = runner.gender;
                                racerInfo.totalRuns = runner.totalRuns ? runner.totalRuns : '-';
                                racerInfo.daysSinceLastRun = runner.daysSinceLastRun;
                                racerInfo.questionAnswer = runner.questionAnswer ? runner.questionAnswer : '-';
                                racerInfo.postQuestionAnswer = runner.postRunAnswer ? runner.postRunAnswer : '-';
                                racerInfo.totalDistance = runner.totalDistance ? runner.totalDistance : '-';
                                racerInfo.isNotSubscribed = runner.unsubscribed ? runner.unsubscribed:false;
                                racerInfo.memo = runner.runnerMemo?runner.runnerMemo.memo:'-';
                                racerInfo.isNew = isNewRunner
                                raceData.racerInfos.push(racerInfo);
                            }, runnersSortByDistance);

                            // raceData.raceStatus = status;
                            var runnersWithRank  = R.reject(R.propEq('rank', '-'))(raceData.racerInfos);
                                runnersWithRank  = R.sortBy(R.prop('rank'), runnersWithRank);
                            var runnersWithoutRank = R.filter(R.propEq('rank', '-'))(raceData.racerInfos);
                            raceData.racerInfos =  runnersWithRank.concat(runnersWithoutRank)
                            cache[raceId] = raceData.racerInfos;
                        
                            consoleRunners[raceId].raceData = raceData;
                            // logs.stop("Getting Race Runners Data ..."+raceId)
                            console.log("startRaceInterval end!")
                            consoleRunners[raceId].isRequestComplete = true;
                            //Clear cache if status of race id finished
                            if(consoleRunners[raceId].raceLiveStatus === 'finished'){
                                clearCache(runnersDatacacheId)
                            }
                            _callback();
                        } else {
                            consoleRunners[raceId].raceData = raceData;
                            consoleRunners[raceId].isRequestComplete = true;
                            _callback();
                        }
                    })
                    
                 }
                 catch(error){
                    consoleRunners[raceId].isRequestComplete = true;
                    logs.error(error)
                    logs.warning('error occured in raceID:'+ raceId)
                 }
                });
            });
        })
        
    }
    catch(error){
        consoleRunners[raceId].isRequestComplete = true;
        logs.warning('error occured in raceID:'+ req.params.raceId)
        logs.error(error)
    }
              
                 
             
}

//Api to set finish race status on fb

router.get('/finishRace/:raceId', function(req,res,next){
        const raceId = req.params.raceId;
        if(raceId){
            finishRace(raceId)
        }
})


// Update finished status on fb
function finishRace(raceId){
    db.ref('racers').child(raceId).update({
        'status': 'finished'
    })
}
//
router.get('/clearCache/:cacheId', function(req,res,next){
    const cacheId = req.params.cacheId;
    var cleared =   clearCache(cacheId);
    res.send(cleared);
});

//Add All Runners to Race in FireBase Database
router.get('/initRace/:raceId', function (req, res, next) {
    const raceId = req.params.raceId;
    const raceInfo = {
        raceId: req.params.raceId,
        authorization: req.header('Authorization')
    }
    getRunner(raceInfo, (data) => {
        runners = JSON.parse(data);
        runners.forEach(function (runner) {

            if(runner!= null){
                db.ref('racers').child(raceId).child("runners").child(runner.id).update({
                    "distance": 0,
                    "finalAltitude": 0,
                    "isRunning": true,
                    "name": runner.firstName,
                    "pace": 0,
                    "time": 0
                })
            }
        });

    });
    res.end();
})


//Add All Runners to Race in Database
router.get('/addRunnerstoRace/:raceId/:limit', function (req, res, next) {
    const raceId = req.params.raceId;
    const limit = req.params.limit;
    const raceInfo = {
        raceId: raceId,
        authorization: req.header('Authorization')
    }
    var idx = 0;
    getAllRunners(raceInfo, (data) => {
        var allRunners = JSON.parse(data);
        allRunners.forEach(function (runner) {
            if (idx < limit) {
                const info = {
                    raceId: raceId,
                    runnerId: runner.id,
                    authorization: req.header('Authorization')
                }
                addAllRunners(info, (data) => {

                })
                idx++;
            }
        });
    })
    res.send('Added ' + limit + ' Runneres to Race ' + raceId);
    res.end();
})

function clearCache(cacheId){
    myCache.del(cacheId, function( err, count ){
        if( !err ){
          console.log( count ); // 1
          // ... do something ...
          return count;
        }
      });
}


function timeLeft(oldDate) {
    var now = new Date();
    var endDate = oldDate; // 2017-05-29T00:00:00Z
    var diff = now - endDate; 
    var minutes = Math.floor((diff % 3.6e6) / 6e4);
    return minutes;
}


function setCurrentTime(){

    myCache.get('fbRaceInfo', function( err, value ){

        if( !err ){
             
            consoleIntervals[value.raceId].interval = setInterval(function(){ 
                if(consoleRunners[value.raceId]!=undefined){
                    if(consoleRunners[value.raceId].timerUpdateDoneinFirebase === undefined){
                        consoleRunners[value.raceId].timerUpdateDoneinFirebase = 1;
                    }
               
                    if(consoleRunners[value.raceId].timerUpdateDoneinFirebase === 1){
                            consoleRunners[value.raceId].timerUpdateDoneinFirebase = 0;


                            db.ref('racers').child(value.raceId).child('status').on('value', (status) => {
                                consoleRunners[value.raceId].raceStatus = status.val();
                            });

                            db.ref('racers').child(value.raceId).child('raceResumeId').on('value', (resumeId) => {
                                ResumeId = resumeId.val();
                            });

                            if(consoleRunners[value.raceId].raceStatus === 'finished'){
                                clearInterval(consoleIntervals[value.raceId].interval)
                            }

                            console.log('startDateTime------> '+value.startDateTime)
                            console.log('raceDuration------> '+value.raceDuration)
                            const then = value.startDateTime + (value.raceDuration * 60) * 1000;

                            const seconds = Math.round((then - Date.now()) / 1000);
                            if(seconds<1 && seconds >-1){
                                const params = {
                                    token: value.token,
                                    id:ResumeId,
                                    raceId:value.raceId
                                }
                                getRaceResume(params, (data) => {
                                    var raceRes = JSON.parse(data);

                                    var raceResume = {
                                                    id: raceRes.id,
                                                    finishDate: new Date().toISOString(),
                                                    startDate: raceRes.startDate,
                                                    race: raceRes.race,
                                                    totalAmountRunners: consoleRunners[value.raceId].totalFbRunners  
                                    };
                                    params['raceResumeData'] = raceResume;
                                    updateRaceResume(params, (data) => {
                                        finishRace(value.raceId)
                                        updateFinishRace(params,(data)=>{
                                        })
                                    })
                                })

                            }
                            if(consoleRunners[value.raceId].raceStatus != 'finished' && seconds >= 0){
                            db.ref('racers').child(value.raceId).update({
                                'currentTime': seconds
                            }).then(function(){
                                console.log('Seconds:'+seconds);
 
                                consoleRunners[value.raceId].timerUpdateDoneinFirebase = 1;
                                
                            })
                            .catch(function(error) {
                                console.log(error);
                                consoleRunners[value.raceId].timerUpdateDoneinFirebase = 1;
                            })   
                    }
                    }
                }
               
            
            }, 1000);
           
        }
    });

}

function includeAllrunners(dbRunners,fbRunner){
    var result = fbRunner.filter(function(o1){
        return !dbRunners.some(function(o2){
            return o1.id === o2.id;   
        })
    });
    return result;
}

//reaceInfo = From Firebase
function getRunner(raceInfo, callback) {
    options.headers.authorization = raceInfo.authorization;
    var opt = options;
    opt.path = '/api/races/' + raceInfo.raceId + '/runners';

    http.get(opt, (res) => {
        var body = "";
        res.on("data", data => {
            body += data;
        });
        res.on("end", () => {

            callback(body);
        });

    }).on('error', (e) => {
        console.error(e);
    });

}


router.get('/diagnose/:raceId', function (req, res, next) {
    const raceId = req.params.raceId;
    const token =  req.header('Authorization');
    const raceParams = {
        raceId: raceId,
        authorization: token
    }

    getRunner(raceParams, (data) => {
        var dbRunners = JSON.parse(data);
            dbRunners = dbRunners.filter(function( e ) {
                    return e !== null;
                });
        var runnerIds = R.project(['id'], dbRunners);
        var ids = runnerIds.map(a => a.id);
        var raceParams = {
            raceId: raceId,
            authorization: token,
            runnerIds: ids
        }

        getRunnersDataoneByone(raceParams, function (runnerInfos,error) {
            
            console.log(runnerInfos)
            res.send(runnerInfos)
        });
    })
})
//modified function to get runnersData
function getRunnersDataoneByone(raceInfo, callback) {
    var t0 =  Date.now();
    var runnersInfo = [];
    var mycount = 0;
    raceInfo.runnerIds.forEach(runnerId => {
       
        var runnerIdArray = [];
        runnerIdArray.push(runnerId)
        var options = {
            method: 'POST',
            url: config.liveApi + raceInfo.raceId,
            headers: {
                authorization: raceInfo.authorization
            },
            body: JSON.stringify(runnerIdArray)
        };
    
        request(options, function (error, response, body) {
            mycount++;
            if (error) throw new Error(error);
            if(response.statusCode == 500){
                runnersInfo.push(runnerId)
                
            }
            // if(JSON.parse(body).hasOwnProperty('message')){
                
            //  }else{
            //     runnersInfo.push(JSON.parse(body)[0])
            //  }
            
             if(raceInfo.runnerIds.length === mycount){
                callback(runnersInfo);
            //     var t1 =  Date.now();
            //     mycount =0
            //     console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.")
            //     callback(runnersInfo);
             }
        });
         
    });
}

//actual function to get runnersData
function getRunnersdata(raceInfo, callback) {  
    var options = {
        method: 'POST',
        url: config.liveApi + raceInfo.raceId,
        headers: {
            authorization: raceInfo.authorization
        },
        body: raceInfo.runnerIds
    };

    if(raceInfo.raceStatus != 'running'){
            request(options, function (error, response, body) {
                if (error) 
                {
                    logs.warning(error)
                    throw new Error(error);
                }
                if(response.statusCode == 500){
                    callback([],JSON.parse(body));
                }else{
                    callback(JSON.parse(body)); 
                }
                
            });
    } else {
            runnersDatacacheId = "getRunnersdata-" + raceInfo.raceId;
                var getData = new Promise(function (resolve, reject) {
                                    //Get Cache Race Runners Data
                                    myCache.get(runnersDatacacheId, function( err, value ){
                                        if( !err ){
                                            if(value == undefined){
                                                resolve([])
                                            }else{
                                                resolve(value)
                                            }
                                        }
                                    });
                                });
                    getData.then(function (data) {
                        var totalIds = JSON.parse(raceInfo.runnerIds).length;
                        var cacheIds = data.length;
                            if(totalIds!=cacheIds){
                                //Get only newely added id data
                                var cacheIdss = R.project(['runner'], data);
                                var cids = cacheIdss.map(a => a.runner.id);
                                var newIds = JSON.parse(raceInfo.runnerIds).filter(function(obj) { return cids.indexOf(obj) == -1; })
                                options.body = JSON.stringify(newIds)
                                if(cacheIds>totalIds){
                                    var leftIds = cids.filter(function(obj) { return JSON.parse(raceInfo.runnerIds).indexOf(obj) == -1; })
                                    var index = data.map(x => {
                                        return x.runner.id;
                                      }).indexOf(leftIds[0]);
                                      
                                      data.splice(index, 1);
                                }
                                request(options, function (error, response, body) {
                                    if (error) throw new Error(error);
                                    if(response.statusCode == 500){
                                        callback([],JSON.parse(body));
                                    }else{
                                        //Set Race Runners Data in Cache
                                       
                                         var finalBody = [].concat(data, JSON.parse(body));
                                        myCache.set(runnersDatacacheId, finalBody, function( err, success ){
                                            if( !err && success ){
                                            callback(JSON.parse(body));
                                            }
                                        });
                                    }
                                    
                                });
                            }else{
                                callback(data)
                            }
                    })
    }
      
}
function getAllRunners(raceInfo, callback) {
    options.headers.authorization = raceInfo.authorization;
    var opt = options;
    opt.path = '/api/runners';
    http.get(opt, (res) => {
        var body = "";
        res.on("data", data => {
            body += data;
        });
        res.on("end", () => {
            callback(body);
        });

    }).on('error', (e) => {
        console.error(e);
    });
}

function addAllRunners(runnerInfo, callback) {
    post_options.path = '/api/races/' + runnerInfo.raceId + '/add-runner/' + runnerInfo.runnerId;
    post_options.headers.authorization = runnerInfo.authorization;
    var req = http.request(post_options, (res) => {
        res.on('data', (d) => {});
        res.on("end", () => {
            callback('added' + runnerInfo.runnerId);
        });
    });
    req.on('error', (e) => {
        console.error(e);
    });
    req.end();
}

function getRaceInfo(raceInfo,callback){
    options.headers.authorization = raceInfo.authorization;
    var opt = options;
    opt.path = '/api/races/' + raceInfo.raceId;
    http.get(opt, (res) => {
        var body = "";
        res.on("data", data => {
            body += data;
        });
        res.on("end", () => {

            callback(body);
        });

    }).on('error', (e) => {
        console.error(e);
    });
}

function pace(meters, minutes) {
    var km = meters / 1000;
    var ritSeg = minutes / km; 
    var paceMin = ritSeg - ritSeg % 1;
    var paceSeg = ritSeg % 1 * 0.6 * 100;
    paceSeg = paceSeg - paceSeg % 1;
  

    // return paceMin+"'"+paceSeg+'"';
    return paceMin*60+paceSeg;
}

function getAnswerByRunner(runnerInfo, callback) {
    options.headers.authorization = raceInfo.authorization;
    var opt = options;
    opt.path = '/api/race-questions/runner-answer/' + raceInfo.raceId+'/'+raceInfo.runnerId;
    http.get(opt, (res) => {
        var body = "";
        res.on("data", data => {
            body += data;
        });
        res.on("end", () => {

            callback(body);
        });

    }).on('error', (e) => {
        console.error(e);
    });
}

function getRaceResume(info, callback){
    options.headers.authorization = info.token;
    var opt = options;
    opt.path = '/api/race-resumes/'+info.id;
    http.get(opt, (res) => {
        var body = "";
        res.on("data", data => {
            body += data;
        });
        res.on("end", () => {

            callback(body);
        });

    }).on('error', (e) => {
        console.error(e);
    });

}

function updateRaceResume(info, callback){
    var options = {
        url: 'https://app.liverunapp.com:443/api/race-resumes',
        method: 'PUT',
        headers: {
            'authorization': '',
            'Content-Type': 'application/json'
        },
        body: info.raceResumeData,
        json: true 
    };
    options.headers.authorization = info.token;
    request(options, function (error, response, body) {
        if (error) throw new Error(error);  
        callback(body)
      });
}

function updateFinishRace(info,callback){
    const post_options = {
        host: 'app.liverunapp.com',
        port: 443,
        method: 'PUT',
        path: '',
        headers: {
            'authorization': '',
            'Content-Type': 'application/json'
        }
    };
    post_options.path = '/api/races/'+info.raceId+'/finish-race';
    post_options.headers.authorization = info.token;
    var req = http.request(post_options, (res) => {
        res.on('data', (d) => {});
        res.on("end", () => {
            callback('updated finish race');
        });
    });
    req.on('error', (e) => {
        console.error(e);
    });
    req.end();
}

module.exports = router;
