
var winston = require('winston');
require('winston-loggly-bulk');
require('winston-timer')(winston);

winston.add(winston.transports.Loggly, {
    token: "0b315f04-d12b-40ac-9f7e-6f145abc03a3",
    subdomain: "sanjaykumar",
    tags: ["LiveRun"],
    color: true,
    json:true
});

function start(taskName)
{
    winston.start_log(taskName, 'info'); 
}

function stop(taskName)
{
    winston.stop_log(taskName, 'info');
}


function info(msg) {
    // console.log(msg)
    winston.log('info',msg);
}

function error(msg) {
    // console.log(msg)
    winston.error('error',msg);
}

function warning(msg) {
    // console.log(msg)
    winston.warn('warning',msg);
}

module.exports.info = info;  
module.exports.warning = warning;  
module.exports.error = error;  
module.exports.start = start;  
module.exports.stop = stop;  

